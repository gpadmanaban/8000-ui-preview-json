var React = require('react');
var location = "";
var BubblePreviewEditor = require('./bubbleprevieweditor.js');
var groupSelfServe;

var ListBubbles = React.createClass({

    getInitialState() {  
    location = this.props.queryParams['payload'];    
        if(location == undefined)
            location = "";
        var isBrands = this.props.isBrands;
        pageType = "brands";
        var renderMethod = this.renderAdaMode;
        return{
            showMore: "hide",
            isBrands: isBrands,
            pageType: pageType,
            renderMethod: renderMethod
        };
    },

    renderAdaMode(){
       return <div>
        <div className="emulator__col emulator__col--1-2">
                {this.getBubbles()}
            </div>
            
        </div>
    },
    getPourBelowSection(){
               
            var pourBelow = 

<div className="pour-controls">
                <div className="pour-sizes section">
                <div className="pour-sizes">

                <div className="pour-size" 
                style={{backgroundImage:"url('/etc/designs/1-0/freestyle-cms-crew-serve/images/ui/size-selected.bmp')"}}>
                <span>K</span>
                </div>

                <div className="pour-size" 
                style={{backgroundImage:"url('/etc/designs/1-0/freestyle-cms-crew-serve/images/ui/size-selected.bmp')"}}>
                <span>S</span>
                </div>

                <div className="pour-size" 
                style={{backgroundImage:"url('/etc/designs/1-0/freestyle-cms-crew-serve/images/ui/size-selected.bmp')"}}>
                <span>M</span>
                </div>

                <div className="pour-size" 
                style={{backgroundImage:"url('/etc/designs/1-0/freestyle-cms-crew-serve/images/ui/size-selected.bmp')"}}>
                <span>L</span>
                </div>

                <div className="pour-size" 
                style={{backgroundImage:"url('/etc/designs/1-0/freestyle-cms-crew-serve/images/ui/size-selected.bmp')"}}>
                <span>XL</span>
                </div>

                </div>
                </div>

<div className="pour-drink-container section">

            <div className="pour-drink">
                <img src={"/etc/designs/1-0/freestyle-cms-crew-serve/images/ui/pour.bmp"}/>
            </div>

        </div>

        <div className="settings-toggle-container section">




            <div className="settings-toggle">
                <img src={"/etc/designs/1-0/freestyle-cms-crew-serve/images/ui/setting.png"}/>
            </div>


        </div>


                </div>





            
        return pourBelow;  
                            
    },

    componentDidMount(){
        if(!this.props.isBeverages){
        this.props.setActiveTabProps();
        var self = this;
        var currentElement;
      
            currentElement = $(".show.foreground").children();
                            
            var editor = <BubblePreviewEditor 
            dataObj={this.props.dataObj} 
             queryParams={this.props.queryParams}
            currentElementId={this.props.dataObj.id} />;
           
        var editorDiv = document.querySelector(".previewEditor");
        React.render(editor, editorDiv);}
    },
    
     getBubbles(){
        var self = this;
        var bubbles = [];
        var dataObj = this.props.dataObj;  
        var beverageClass = "";
        if(this.state.isBeverages){
            beverageClass = "beverage drink-bubble ";
        }    
        dataObj.sort(function(a, b) {
            if(a.displayOrder && b.displayOrder)
                return (a.displayOrder[0].priority - b.displayOrder[0].priority);
        });
        if(localStorage.getItem("hiddenIds")){
        var bubblesCount = dataObj.length - localStorage.getItem("hiddenIds").split(",").length;}
        else {
            bubblesCount = dataObj.length;
        }
        dataObj.forEach(function(element, index) {
            var hiddenBubbles;
         if(localStorage.getItem("hiddenIds")){
               hiddenBubbles = localStorage.getItem("hiddenIds").split(","); }

            var isHidden = false;
            if(hiddenBubbles && hiddenBubbles != "" && hiddenBubbles.indexOf(element.id) > -1)
            {    
            for(var i=0; i< hiddenBubbles.length;i++){
                if(element.id === hiddenBubbles[i]){
                    isHidden = true;
                }
            }}



            if(!isHidden){
                //if(element.subtype == "brand" || element.subtype == "flavor" || element.subtype == "promotion" || element.subtype == "lto" || element.subtype == "beverage"){
                    var image = "";
                    if(element.displayElements && element.displayElements[0].image)
                        image = element.displayElements[0].image;
                    else if(element.displayStates && element.displayStates[0].image)
                        image = element.displayStates[0].image;

                    var dimmedIds;
                    if(localStorage.getItem("dimmedIds")){
                    dimmedIds = localStorage.getItem("dimmedIds").split(","); 
               
                    if(dimmedIds && dimmedIds != "" && dimmedIds.indexOf(element.id) > -1){
                            image = element.displayStates[1].image;
                        }}
                        var src = location + image; 
                        var drinkClass;
                        if(bubblesCount > 31)                      
                            drinkClass = "drink drink--small ";
                        else                        
                            drinkClass = "drink drink--large ";             
                   
                        bubbles.push(
                            <a className={drinkClass + "selectable-js "} data-element-id={element.id} href="#" onClick={(e) => this.props.callback(element.id,element.subtype)}>
                                <img src={src} />
                            </a>   
                        )
                    }
        }, this);
        var section = [];
        self.paginateBubbles(bubbles);
     
        
        this.state.bubblesPaginated.forEach(function(paginated, index){
            var show = index == 0 ? "show" : "hide";            
            //Set bubble space classes
            var bubbleSpaceClass = "xsmall-bubbles";
             
                
            bubbleSpaceClass += " bubble-grid-pop";

           
                var paginateBack = <div></div>;
                var paginateMore = <div></div>;
               
                section.push(<div className={"bubble-page " + show}>                    
                    <div className={"bubble-container "}>                   
                        {paginateBack} 
                        {paginated}      
                        {paginateMore}                      
                    </div>
                </div>);
              
                   
        });
        if(this.state.bubblesPaginated.length > 1)
            self.state.showMore = "show";
        return section;
    },

    paginateBubbles(bubbles) {
        var pageType = this.state.pageType;
        this.state.bubblesPaginated = [];
        var page = 0;
        
        if (pageType === 'brands') {
            for (var i = 0; i < bubbles.length; i++) {
                if (!this.state.bubblesPaginated[page]) {
                    this.state.bubblesPaginated[page] = [];
                }

                this.state.bubblesPaginated[page].push(bubbles[i]);

                if (bubbles.length > 35) {
                   
                        if (i === 32)
                            page = 1;
                    
                    
                }
            }
        }
      
    },

  

   

    render() {
        return (
            <div data-element-id={this.props.activeId} 
            data-group-id={this.props.groupId} 
            data-group-name={this.props.groupNameString} 
            data-page-type={this.state.pageType} 
            data-sub-type={this.props.subtype} 
            data-prev-tab={this.props.prevTab}>                
                {this.state.renderMethod()}
                {this.getPourBelowSection()}
              
              

                <div className="previewEditor"></div>
            </div>
        );
    }

});

module.exports = ListBubbles;
