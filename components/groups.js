var React = require('react');
var ListBubbles = require('./listbubbles.js');
var location = "";
var bundleVersion = "";
var Group = React.createClass({

    getInitialState() {
    bundleVersion = this.getbundleVersion();
       var renderMethod = this.renderAdaMode;
       location = this.props.queryParams['payload'];
       if(location === undefined){
        location = "";
       }
        return{
            selectedBrands: [],
           renderMethod: renderMethod,
        };
    },

  componentDidMount(){
        var self = this;       
            //$(".emulator-preview-bar-option.groupOption").removeClass("show").removeClass("hide").addClass("show");
            //$(".emulator-preview-bar-option.bubbleOption").removeClass("hide").removeClass("show").addClass("hide");
        
        this.openActiveTab();
    }, 

    getbundleVersion(){
        var url = window.location.href;
        var urlarr = url.split("/");
        //remove the last html path
        urlarr.pop();
        var bundleVersion = urlarr[urlarr.length-1];
        return bundleVersion;
    },

    openActiveTab(){
        var self = this;
       self.selectGroup();
                },

    showPourBelowSection(){
        $('.pour-controls').css("display","block");
    },

    selectGroup(){
      var listSection = document.getElementById('ListBubbles');
      listSection.className = listSection.className.replace("hide", "show");
        var groups = document.getElementById("Groups");
        groups.className = groups.className.replace("show", "hide");
        var brands = <ListBubbles setActiveTabProps={this.setActiveTabProps} 
        isBrands={true} 
        queryParams={this.props.queryParams}
        dataObj={this.props.brandsData.elements} 
        callback={this.selectBrand} />;
        React.render(brands,listSection);
        if(localStorage.getItem("brandId")){
            this.selectBrand(localStorage.getItem("brandId"), "brand");
        }
    },

    selectBrand(brandId,subtype){ 
        this.showPourBelowSection();
        //localStorage.setItem("brandId", brandId);
       var beverageSection = document.getElementById('BeverageList');
        beverageSection.className = beverageSection.className.replace("hide", "show");
        var groups = document.getElementById("Groups");
        groups.className = groups.className.replace("show", "hide");
        var brandObjCollection = this.getSelectedBeverages(brandId,this.props.brandsData.elements); 
        var beverages = <ListBubbles 
        queryParams={this.props.queryParams}
        setActiveTabProps={this.setActiveTabProps}  
        subtype={subtype}
        isBeverages={true} 
        dataObj={brandObjCollection}/>;        
        React.render(beverages, beverageSection);
    },

    getSelectedBeverages(brandId,brands){
   
        var selectedBeverages = [];
        var beverages = this.props.beveragesData.elements;
        var background = "";
        var preselect = "";
        var brandObjCollection = {};
        brands.forEach(function (obj) {               
            if(obj.id == brandId){
                if(obj.elements){
                    obj.elements.forEach(function (bevObj) {
                        beverages.forEach(function (beverage){     
                            if (bevObj.refId == beverage.id) {
                                selectedBeverages.push(beverage);                        
                            }
                        });   
                    });   
                }
               
        }});
       
        return selectedBeverages;
    },    
    

    setActiveTabProps(mode){        
        var activeTabProps = {};
        var activeTab = "";
            activeTab = $(".foreground.show").attr("id");
        activeTabProps["activeId"] = $("#" + activeTab).children().attr("data-element-id");
        activeTabProps["groupId"] = $("#" + activeTab).children().attr("data-group-id");
        activeTabProps["groupNameString"] = $("#" + activeTab).children().attr("data-group-name");
        activeTabProps["subtype"] = $("#" + activeTab).children().attr("data-sub-type");
       
        //localStorage.setItem(bundleVersion + "_activeTabProps", JSON.stringify(activeTabProps));
    },

    hidePreviewBar(){
        $("#emulator-preview-bar").removeClass("show").addClass("hide");
        $(".open-emulator-preview-bar").removeClass("hide").addClass("show");
        $("#GroupSection").css("margin","45px auto");
    },

    showPreviewBar(){
        $("#emulator-preview-bar").removeClass("hide").addClass("show");
        $(".open-emulator-preview-bar").removeClass("show").addClass("hide");
        $("#GroupSection").css("margin","0px");
    },

    renderAdaMode(){
        var self = this;
        var groupSection = <div id="ada-emulator-container" style={{display: "block"}}>
            <div className="emulator" style={{backgroundImage: "url('/content/dam/freestyle-assets/master-images/us-united-states/background-screens/8000-backgrounds/background.bmp')"}}>    
                <div className="ada-background"></div>        
                <div id="Groups" className="foreground show">
                                      
                    <div className="emulator__col emulator__col--1-2">           
                       
                      
                    </div>
                    
                </div>
                <div id="ListBubbles" className="foreground">

                </div>
                <div id="BeverageList" className="foreground hide">
                </div>

            </div>
        </div>;
        return groupSection;
    },

    openBubblePreviewEditor(){
       $(".bubble-preview-editor").removeClass("hide").addClass("show");
        },

    render() {
        return (            
            <div>
                

                                    
             
                <div id='GroupSection'>
                    {
                        this.state.renderMethod()
                    }                 
                </div>
            </div>
        );
    }

});

module.exports = Group;
