var React = require('react');
var location = "";
var parentClass = "";

var CE = React.createClass({    
        getInitialState() {
            location = this.props.queryParams['payload'];
       if(location === undefined){
        location = "";
       }
         parentClass = ".primary-category-frame.show";
           
            return {};    
        },

        componentDidMount(){          
            var editorChecks = $(".bubble-preview-editor-item-visibility input");
            var hiddenBubbles = localStorage.getItem("hiddenIds");
            for(var i=0; i<editorChecks.length; i++){
                var elementId = $(editorChecks[i]).attr("data-element-id");
                if(hiddenBubbles && hiddenBubbles.indexOf(elementId) > -1){
                    $(editorChecks[i]).attr("checked","checked");
                }
                else
                    $(editorChecks[i]).removeAttr("checked");                
            }

            if(localStorage.getItem("dimmedIds")){
                var dimmedIds = localStorage.getItem("dimmedIds").split(",");
                for(var i=0; i<dimmedIds.length;i++){
                    $('#'+dimmedIds[i]).val("dimmed"); 
                }
               
            }
        },
        
        closeBubblePreviewEditor(){
           
        $(".bubble-preview-editor").removeClass("show").addClass("hide");
        },
        
        renderPreviewBubbles(){
            var previewBubbles = [];
            this.props.dataObj.forEach(function(element, index) {
                var imgSrc = "";
                if(element.displayElements && element.displayElements[0].image)
                    imgSrc = element.displayElements[0].image;
                else if(element.displayStates && element.displayStates[0].image)
                    imgSrc = element.displayStates[0].image;
                imgSrc = location + imgSrc;
                var elementId = element.id;
                previewBubbles.push(
                    <div className="bubble-preview-editor-item">
                        <div className="bubble-preview-editor-item-image">          
                            <img src={imgSrc} />
                        </div>
                        <div className="bubble-preview-editor-item-states" data-element-id={elementId}>
                            <select id={elementId}>
                                <option value="available" selected="">Available</option>    
                                <option value="dimmed" selected="">Dimmed</option>              
                            </select>
                        </div>
                        <div className="bubble-preview-editor-item-visibility">
                            <label><input type="checkbox" data-element-id={elementId} className="bubble-preview-editor-item-visibility-input" /> Hide</label>
                        </div>
                        <div className="bubble-preview-editor-item-warning">            
                        </div>
                    </div> 
                )
            });
            return previewBubbles;
        },

        savePreviewBubbles(){
            var hiddenBubbles = $(".bubble-preview-editor-item-visibility input:checked");

            var hiddenIds = [];
            for(var i=0; i<hiddenBubbles.length; i++){
                var elementId = $(hiddenBubbles[i]).attr("data-element-id");
                hiddenIds.push(elementId);                
            }
            localStorage.setItem("hiddenIds", hiddenIds);
            var dimmedIds= [];

            for(var i=0; i<this.props.dataObj.length; i++){
                var currentId = this.props.dataObj[i].id;
            if($('#'+ currentId + ' option:selected').val() == "dimmed"){
                   dimmedIds.push(this.props.dataObj[i].id);
                }
                
            }
            localStorage.setItem("dimmedIds", dimmedIds);
            window.location.reload();


        },
            
        render() {
            return (
                <div className="bubble-preview-editor emulator-modal hide">
                    <div id="bubble-preview-editor-content">
                        {this.renderPreviewBubbles()}
                    </div>
                    <div className="bubble-preview-editor-controls">
                        <button onClick={this.closeBubblePreviewEditor} className="bubble-preview-editor-cancel-button bubble-preview-editor-controls-button--cancel bubble-preview-editor-controls-button">Cancel</button>
                        <button onClick={this.savePreviewBubbles} className="bubble-preview-editor-save-button bubble-preview-editor-controls-button">Save</button>
                    </div>
                </div>
            );
        }
    
    });
    
    module.exports = CE;
    
