var React = require('react');
var Group = require('./groups');
$ = require('jquery');
var version = "";

var App = React.createClass({

    
        getInitialState() {
            var searchParams = window.location.search.split("&");
            var queryParams = {};
            searchParams.forEach(function(param,index){
                var params = param.split("=");
                if(index == 0)
                    params[0] = params[0].replace("?","");
                queryParams[params[0]] = params[1];
            });
            version = queryParams['payload'];
            if(version == undefined)
                version = "";
           var contentJson = this.GetJsonData("/metadata/content.json");
            var title = contentJson.content.elements[0].name;
            var groupJson = this.GetJsonData("/metadata/groups.json");
            var brandJson = this.GetJsonData("/metadata/brands.json");
            var beverageJson = this.GetJsonData("/metadata/beverages.json");
            
            var languageConfig = this.GetJsonData("/metadata/language-configurations.json");
            var languages = this.getLanguageData(languageConfig);
           
            return {
                groupsData: groupJson,
                brandsData: brandJson,
                beverageJson: beverageJson,
                languagesJson: languages,
                title:title,
                languageConfig: languageConfig,
                 version: version,
                queryParams: queryParams,
                
            };
    
        },

        getLanguageData(langconfig){
            var languages = {};
            langconfig.elements.forEach(function(element) {
                var fileName = element.refId + ".json";
                var jsonData = this.GetJsonData("/languages/" + fileName);
                languages[jsonData.name] = jsonData;
            }, this); 
            return languages;                       
        },
    
        GetJsonData(url){
            var jsonData;
            if(url && url != ""){
                $.ajax({
                    url: window.location.href.split(window.location.pathname)[0] + version + url,         
                    dataType: "json",
                    type: 'GET',
                    async: false,
                    success: function(data){
                        jsonData = data;
                    }
                });
            }
            return jsonData;
        },
    
        render() {
            return (
                <div className="app">
                    <div className="nav">
                        <div className="title">{this.state.title}</div>
                    </div>
                    <Group bundleName={this.state.bundleName}
                    queryParams={this.state.queryParams}
                    languageConfig={this.state.languageConfig} 
                    
                    beveragesData={this.state.beverageJson} 
                    brandsData={this.state.brandsData} 
                    
                    languagesObj={this.state.languagesJson} 
                    groupsData={this.state.groupsData} 
                     />
                </div>
            );
        }
    
    });
    
    module.exports = App;
    
